const HOOF_UP_TEXT = "Brohoof";
const HOOF_DOWN_TEXT = "Hoof down";

const PROCESSED_CLASS = 'processed-disquus';

var body = $('body:first');

/*
  Observe mutations of posts in Disqus iframe
*/
new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        var node;
        $.each(mutation.addedNodes, function () {
            node = $(this).find("#post-list");
            if(node.length) {
              observePostList(node);
            }
        })
    })
}).observe(body[0], {childList: true});

var observePostList = function(postList) {
  new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        var node;
        for (var i = 0; i < mutation.addedNodes.length; i++) {
          node = $(mutation.addedNodes[i]);
          if (node.hasClass("post")) {
            processPost(node);
          } else if (node.hasClass('post-content')) {
            processPost(node.parents('.post'));
          }
        }
      })
  }).observe(postList[0], {childList: true, subtree: true, attributes: true});
}

/*
  Process a Disqus post and change the votes to hooves
*/
var processPost = function(post) {
  var postContent = post.find('.post-content:first');
  var upvote = post.find('.vote-up:first');
  var downvote = post.find('.vote-down:first');
  var upvoteIcon = upvote.find('i.icon-arrow-2:first');
  var downvoteIcon = downvote.find('i.icon-arrow:first');

  // Process all replies
  $.each(post.children('div.children').find('li.post'), function() {
    processPost($(this));
  });

  // Skip minimised posts
  if (post.hasClass("minimized") || postContent.hasClass(PROCESSED_CLASS)) {
    return;
  }

  // Mark as processed
  postContent.addClass(PROCESSED_CLASS);

  // Add brohooves
  upvote.attr('title', HOOF_UP_TEXT);
  downvote.attr('title', HOOF_DOWN_TEXT);
  loadAfter('templates/brohoof-left.html', upvoteIcon, function(brohoofUp) {
    upvoteIcon.hide();
    loadAfter('templates/brohoof-right.html', downvoteIcon, function(brohoofDown) {
      downvoteIcon.hide();
      var updateBrohooves = function() {
        setTimeout(function() {
          if (upvote.hasClass('upvoted')) {
            brohoofUp.removeClass('hoof').addClass('uphoof');
            brohoofUp.parent().addClass('bright');
          } else {
            brohoofUp.removeClass('uphoof').addClass('hoof');
            brohoofUp.parent().removeClass('bright');
          }

          if (downvote.hasClass('downvoted')) {
            brohoofDown.removeClass('hoof').addClass('downhoof');
            brohoofDown.parent().addClass('bright');
          } else {
            brohoofDown.removeClass('downhoof').addClass('hoof');
            brohoofDown.parent().removeClass('bright');
          }
        }, 50)};

      downvote.click(updateBrohooves);
      upvote.click(updateBrohooves);
      updateBrohooves();
    });
  });
}

/*
  Gets post element from a child of the post
*/
var postOf = function(postChild) {
  return postChild.parents('.post:first');
}
